import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-dm-control-access-vcard-styles.js';
import '@vcard-components/cells-util-behavior-vcard';
/**
This component ...

Example:

```html
<cells-dm-control-access-vcard></cells-dm-control-access-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
const utilBehavior = CellsBehaviors.cellsUtilBehaviorVcard;
export class CellsDmControlAccessVcard extends utilBehavior(LitElement) {
  static get is() {
    return 'cells-dm-control-access-vcard';
  }

  // Declare properties
  static get properties() {
    return {
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.addEventListener(this.events.logOut, (event)=>{
      console.log('LogOut', event);
      window.sessionStorage.removeItem(this.ctts.keys.userSesion);
      this.navigate('/');
    });
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-dm-control-access-vcard-shared-styles').cssText}
    `;
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsDmControlAccessVcard.is, CellsDmControlAccessVcard);
